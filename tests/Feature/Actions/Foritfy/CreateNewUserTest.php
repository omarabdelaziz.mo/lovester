<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

use function Pest\Faker\faker;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

uses(RefreshDatabase::class);

it('registers a new user', function () {
    $email = faker()->email;
    $res = $this->post('/register', [
        'name' => 'John Doe',
        'email' =>  $email,
        'password' => 'secret123',
        'password_confirmation' => 'secret123',
        'date_of_birth' => '1990-01-01',
        'gender' => 'male',
        'terms' => true,
    ]);
    $res->assertStatus(302);

    $res->assertRedirect('/dashboard');
    //check for errors
    $res->assertSessionHasNoErrors();

    assertDatabaseHas('users', [
        'name' => 'John Doe',
        'email' => $email,
        'date_of_birth' => '1990-01-01',
    ]);
});


it('fails to register a new user', function () {
    $email = faker()->email();
    $res = $this->post('/register', [
        'name' => 'John Doe',
        'email' =>  $email,
        'password' => 'secret123',
        'password_confirmation' => 'secret1234',
        'date_of_birth' => '1990-01-01',
        'gender' => 'male',
        'terms' => false,
    ]);
    $res->assertStatus(302);

    //check for errors
    $res->assertSessionHasErrors();
    $res->assertSessionHasErrors('terms');

    assertDatabaseMissing('users', [
        'name' => 'John Doe',
        'email' => $email,
        'date_of_birth' => '1990-01-01',
    ]);
});


it('validates the registration form', function () {
    $res = $this->post('/register', [
        'name' => '',
        'email' =>  'test',
        'password' => 'secret123',
        'password_confirmation' => 'secret1234',
        'date_of_birth' => '1990-01-01',
        'terms' => false,
    ]);
    $res->assertStatus(302);

    //check for errors
    $res->assertSessionHasErrors();
    $res->assertSessionHasErrors('name');
    $res->assertSessionHasErrors('email');
    $res->assertSessionHasErrors('password');
    $res->assertSessionHasErrors('terms');

});
