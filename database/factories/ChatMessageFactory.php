<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ChatMessage;
class ChatMessageFactory extends Factory
{

    protected $model = ChatMessage::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'from_id' => $this->faker->randomElement([
                auth()->id(),
                $this->faker->randomElement(auth()->user()->matchedUsers()->get()->pluck('id')->toArray()),
            ]),
            'to_id' => $this->faker->randomElement([
                auth()->id(),
                $this->faker->randomElement(auth()->user()->matchedUsers()->get()->pluck('id')->toArray()),
            ]),
            'message' => $this->faker->sentence,
        ];
    }
}
