<?php

namespace App\Actions\Users;

use App\Models\User;
use Lorisleiva\Actions\Concerns\AsAction;

class DislikeUser
{
    use AsAction;

    public function handle(User $user)
    {
        auth()->user()->swipes()->create([
            'target_id' => $user->id,
            'liked' => false,
        ]);
    }
}
