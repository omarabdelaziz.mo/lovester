<?php

namespace App\Actions\Notifications;

use App\Http\Resources\NotificationResource;
use Inertia\Inertia;
use Lorisleiva\Actions\Concerns\AsController;

class ListMyNotifications
{
    use AsController;

    public function handle()
    {
        //mark all notifications as read
        auth()->user()->unreadNotifications->markAsRead();
        $notifications = NotificationResource::collection(auth()->user()->notifications()->latest()->paginate(10));

        if (request()->wantsJson()) {
            return $notifications;
        }

        return Inertia::render('Notifications/List', compact('notifications'));
    }
}
