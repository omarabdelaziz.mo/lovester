<?php

namespace App\Actions\PrivateChat;

use Lorisleiva\Actions\Concerns\AsController;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\MessageResource;
use App\Events\NewPrivateMessage;

class SendConversation
{
    use AsController;

    public function handle(Request $request, User $match)
    {

        $request->validate([
            'message' => 'required|string|max:255',
        ]);

        $message = ChatMessage::create([
            'from_id' => auth()->id(),
            'to_id' => $match->id,
            'message' => request('message'),
        ]);
        
        broadcast(new NewPrivateMessage($message));

        return response()->json([
            'message' => MessageResource::make($message),
        ]);
    }

}
