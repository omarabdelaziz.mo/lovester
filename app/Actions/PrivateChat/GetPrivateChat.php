<?php

namespace App\Actions\PrivateChat;

use App\Http\Resources\UserResource;
use Inertia\Inertia;
use Lorisleiva\Actions\Concerns\AsAction;
use Lorisleiva\Actions\Concerns\AsController;
use App\Models\ChatMessage;
use App\Models\User;

class GetPrivateChat
{
    use AsController;

    public function handle()
    {
        // get matched users
        $matches = $this->getMatchedUsers();

        // retrieve unread message with user
        $unreadMessages = $this->getUnreadMessages();

        $matches = $matches->map(function ($match) use ($unreadMessages) {
            $match->unread_messages_count= $unreadMessages->where('from_id', $match->id)->count();
            return $match;
        })->sortByDesc('unread_messages_count');

        return Inertia::render('PrivateChat/Index', [
            'matches' => UserResource::collection($matches),
        ]);
    }

    protected function getUnreadMessages()
    {
        return ChatMessage::query()
            ->selectRaw('from_id, count(*) as unread_messages')
            ->where('to_id', auth()->id())
            ->where('read', false)
            ->groupBy('from_id')
            ->get();
    }

    protected function getMatchedUsers()
    {
        $likedUser = auth()->user()->likes()->get();
        $likedByUser = auth()->user()->likesOf()->get();

        $matchedUsersIds = $likedUser->pluck('target_id')->intersect($likedByUser->pluck('user_id'));

        return User::whereIn('id', $matchedUsersIds)->get();
    }
}
