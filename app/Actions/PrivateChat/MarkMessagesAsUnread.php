<?php

namespace App\Actions\PrivateChat;

use Lorisleiva\Actions\Concerns\AsController;
use App\Models\ChatMessage;
use App\Models\User;
use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;

class MarkMessagesAsUnread
{
    use AsController;

    public function handle(User $match)
    {
        $updated = $match->messages()->where('to_id', auth()->id())->update(['read' => true]);
        $messages = ChatMessage::where('from_id', $match->id)->where('to_id', auth()->id())->get();
        return response()->json([
            'messages' => MessageResource::collection($messages),
            'match' => UserResource::make($match),
        ]);
    }
}
