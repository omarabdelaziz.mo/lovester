<?php

namespace App\Actions\PrivateChat;

use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use App\Models\ChatMessage;
use App\Models\User;
use Inertia\Inertia;
use Lorisleiva\Actions\Concerns\AsAction;
use Lorisleiva\Actions\Concerns\AsController;
class GetConversation
{
    use AsController;

    public function handle(User $match)
    {

        //user messages
        $messages = ChatMessage::with(['from', 'to'])->where(function ($query) use ($match) {
            $query->where('from_id', auth()->id());
            $query->where('to_id', $match->id);
        })->orWhere(function ($query) use ($match) {
            $query->where('from_id', $match->id);
            $query->where('to_id', auth()->id());
        })->latest()->get();

        if (request()->ajax()) {
            return response()->json([
                'messages' => MessageResource::collection($messages),
                'match' => UserResource::make($match),
            ])->header('Content-Type', 'application/json');
        }
       return Inertia::render('PrivateChat/Conversation', [
            'messages' => MessageResource::collection($messages),
        ]);
    }
}
