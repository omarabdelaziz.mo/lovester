<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $gender
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $personalAccessTokens
 * @property-read int|null $personal_access_tokens_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @property-read int|null $unreadNotificationsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Fortify\TwoFactorToken[] $twoFactorTokens
 * @property-read int|null $two_factor_tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Profile[] $profiles
 * @property-read int|null $profiles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Team[] $teams
 * @property-read int|null $teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $sentMessages
 * @property-read int|null $sent_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $receivedMessages
 * @property-read int|null $received_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $unreadMessages
 * @property-read int|null $unread_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $readMessages
 * @property-read int|null $read_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $deletedMessages
 * @property-read int|null $deleted_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $archivedMessages
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'date_of_birth'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    public function swipes()
    {
        return $this->hasMany(Swipe::class);
    }

    public function likes()
    {
        return $this->hasMany(Swipe::class)->where('liked', true);
    }

    public function dislikes()
    {
        return $this->hasMany(Swipe::class)->where('liked', false);
    }

    public function likesOf()
    {
        return $this->hasMany(Swipe::class, 'target_id')->where('liked', true);
    }

    public function dislikesOf()
    {
        return $this->hasMany(Swipe::class, 'target_id')->where('liked', false);
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class, 'from_id')->orWhere('to_id', $this->id);
    }
}
