<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Swipe extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'target_id',
        'liked'
    ];

    public function base()
    {
        return $this->belongsTo(User::class);
    }

    public function target()
    {
        return $this->belongsTo(User::class, 'target_id');
    }

    public function scopeLiked($query)
    {
        return $query->where('liked', true);
    }

    public function scopeNotLiked($query)
    {
        return $query->where('liked', false);
    }

    public function scopeNotSwiped($query)
    {
        return $query->whereDoesntHave('swipes', function ($query) {
            $query->where('liked', true);
        });
    }
}
