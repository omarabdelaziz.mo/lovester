<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * App\Models\ChatMessage
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read \App\Models\User $from
 * @property-read \App\Models\User $to
 */
class ChatMessage extends Model
{
    use HasFactory;

    protected $fillable = [
        'from_id',
        'to_id',
        'message',
    ];

    protected $casts = [
        'from_id' => 'integer',
        'to_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function from()
    {
        return $this->belongsTo(User::class, 'from_id');
    }


    public function to()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d H:i:s');
    }

    public function mySentMessages()
    {
        return $this->where('from', auth()->id());
    }

    public function myReceivedMessages()
    {
        return $this->where('to', auth()->id());
    }


    public function scopeMySentMessages($query)
    {
        return $query->where('from', auth()->id());
    }

    public function scopeMyReceivedMessages($query)
    {
        return $query->where('to', auth()->id());
    }

    public function scopeMyMessages($query)
    {
        return $query->where('from', auth()->id())->orWhere('to', auth()->id());
    }

}
