<?php

use App\Actions\Users\GetUsers;
use App\Actions\Users\LikeUser;
use App\Actions\Users\DislikeUser;
use App\Actions\Notifications\ListMyNotifications;
use App\Actions\PrivateChat\GetConversation;
use App\Actions\PrivateChat\GetPrivateChat;
use App\Actions\PrivateChat\SendConversation;
use App\Actions\PrivateChat\MarkMessagesAsUnread;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->check()) {
        return redirect()->route('dashboard');
    }
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', GetUsers::class)->name('dashboard');
    Route::group(['prefix' => 'users'], function () {
        Route::post('/{user}/like', LikeUser::class)->name('users.like');
        Route::post('/{user}/dislike', DislikeUser::class)->name('users.dislike');
    });
    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', ListMyNotifications::class)->name('notifications.index');
    });

    Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
        Route::group(['prefix' => 'private', 'as' => 'private.'], function () {
            Route::get('/', GetPrivateChat::class)->name('index');
            Route::get('/conversation/{match}', GetConversation::class)->name('show');
            Route::post('/conversation/{match}', SendConversation::class)->name('store');
            Route::put('/unread/{match}', MarkMessagesAsUnread::class)->name('unread');
        });
    });
});
